# eth-ws-someip

Automotive Ethernet SOME/IP-SD Wireshark LUA dissectors (Autosar 4.2)

## Installation
In order to use this LUA plugins, they need to be added to Wireshark's 'personal plugins' folder.
### Windows
Copy files to $HOME\AppData\Roaming\Wireshark\plugins\2.6\epan

### Linux
If you prefer not to directly copy your dissector files there, this is the option I like best (assuming you are a Linux user too) :
- clone this repository to your preferred location : **\<repo_location>**
- identify where Wireshark expects to find user created plugins
    - Help -> About -> Folders 
    - on v.2.4.4 this folder is **/$HOME/.config/wireshark/plugins**
- create some symlinks from **$HOME/.config/wireshark/plugins** to **\<repo_location>**
  - $ ln -s \<repo_location>/someip.lua ~/.config/wireshark/plugins/
  - $ ln -s \<repo_location>/sd.lua ~/.config/wireshark/plugins/
  - $ ln -s \<repo_location>/sd_entries.lua ~/.config/wireshark/plugins/
  - $ ln -s \<repo_location>/sd_options.lua ~/.config/wireshark/plugins/
  
Original work: https://github.com/jamores/eth-ws-someip